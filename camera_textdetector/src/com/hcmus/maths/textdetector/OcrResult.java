package com.hcmus.maths.textdetector;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;

import com.googlecode.leptonica.android.Pix;

/**
* Encapsulates the result of OCR.
*/
public class OcrResult {
	private Bitmap bitmap;
	private String text;

	private int[] wordConfidences;
	private int meanConfidence;

	private List<Rect> wordBoundingBoxes;
	private List<Rect> characterBoundingBoxes;
	private List<Rect> textlineBoundingBoxes;
	private List<Rect> regionBoundingBoxes;

	private long timestamp;
	private long recognitionTimeRequired;

	private Paint paint;

	private Pix thresholdedImage;
	
	private Pix greyImage;

	public OcrResult(Bitmap bitmap,
	String text,
	int[] wordConfidences,
	int meanConfidence,
	List<Rect> characterBoundingBoxes,
	List<Rect> textlineBoundingBoxes,
	List<Rect> wordBoundingBoxes,
	List<Rect> regionBoxes, 
	long recognitionTimeRequired) {
		this.bitmap = bitmap;
		this.text = text;
		this.wordConfidences = wordConfidences;
		this.meanConfidence = meanConfidence;
		this.characterBoundingBoxes = characterBoundingBoxes;
		this.textlineBoundingBoxes = textlineBoundingBoxes;
		this.wordBoundingBoxes = wordBoundingBoxes;
		this.regionBoundingBoxes = regionBoxes;
		this.recognitionTimeRequired = recognitionTimeRequired;
		this.timestamp = System.currentTimeMillis();

		this.paint = new Paint();
	}

	public OcrResult() {
		timestamp = System.currentTimeMillis();
		this.paint = new Paint();
	}

	public Bitmap getBitmap() {
		if (characterBoundingBoxes.isEmpty()) {
			return bitmap;
		} else {
			return getAnnotatedBitmap();
		}
	}

	private Bitmap getAnnotatedBitmap() {
		Canvas canvas = new Canvas(bitmap);

		// Draw bounding boxes around each word
		for (int i = 0; i < wordBoundingBoxes.size(); i++) {
			paint.setAlpha(0xA0);
			paint.setColor(0xFF00CCFF);
			paint.setStyle(Style.STROKE);
			paint.setStrokeWidth(3);
			Rect r = wordBoundingBoxes.get(i);
			canvas.drawRect(r, paint);
		}    

		//    // Draw bounding boxes around each character
		//    for (int i = 0; i < characterBoundingBoxes.size(); i++) {
		//      paint.setAlpha(0xA0);
		//      paint.setColor(0xFF00FF00);
		//      paint.setStyle(Style.STROKE);
		//      paint.setStrokeWidth(3);
		//      Rect r = characterBoundingBoxes.get(i);
		//      canvas.drawRect(r, paint);
		//    }

			return bitmap;
	}

	public String getText() {
		return text;
	}

	public int[] getWordConfidences() {
		return wordConfidences;
	}

	public int getMeanConfidence() {
		return meanConfidence;
	}

	public long getRecognitionTimeRequired() {
		return recognitionTimeRequired;
	}

	public Point getBitmapDimensions() {
		return new Point(bitmap.getWidth(), bitmap.getHeight()); 
	}

	public List<Rect> getCharacterBoundingBoxes() {
		return characterBoundingBoxes;
	}

	public List<Rect> getTextlineBoundingBoxes() {
		return textlineBoundingBoxes;
	}

	public List<Rect> getWordBoundingBoxes() {
		return wordBoundingBoxes;
	}

	public List<Rect> getRegionBoundingBoxes() {
		return regionBoundingBoxes;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setWordConfidences(int[] wordConfidences) {
		this.wordConfidences = wordConfidences;
	}

	public void setMeanConfidence(int meanConfidence) {
		this.meanConfidence = meanConfidence;
	}

	public void setRecognitionTimeRequired(long recognitionTimeRequired) {
		this.recognitionTimeRequired = recognitionTimeRequired;
	}

	public void setCharacterBoundingBoxes(List<Rect> characterBoundingBoxes) {
		this.characterBoundingBoxes = characterBoundingBoxes;
	}

	public void setTextlineBoundingBoxes(List<Rect> textlineBoundingBoxes) {
		this.textlineBoundingBoxes = textlineBoundingBoxes;
	}

	public void setWordBoundingBoxes(List<Rect> wordBoundingBoxes) {
		this.wordBoundingBoxes = wordBoundingBoxes;
	}

	public void setRegionBoundingBoxes(List<Rect> regionBoundingBoxes) {
		this.regionBoundingBoxes = regionBoundingBoxes;
	}

	public Pix getThresholdedImage(){
		return this.thresholdedImage;
	}
	
	public void setThresholdedImage(Pix thresholdedImage){
		this.thresholdedImage = thresholdedImage;
	}
	
	public Pix getGreyImage(){
		return this.greyImage;
	}
	
	public void setGreyImage(Pix greyImage){
		this.greyImage = greyImage;
	}

	@Override
	public String toString() {
	return text + " " + meanConfidence + " " + recognitionTimeRequired + " " + timestamp;
	}
}
