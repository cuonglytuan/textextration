package com.hcmus.maths.textdetector;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TextUtil{

	public static String caseDiacriticIgnore(String input){
		String normalized = Normalizer.normalize(input, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		Map<Character, Character> charReplacementMap = new HashMap<Character, Character>();
		charReplacementMap.put('Đ', 'D');
		charReplacementMap.put('đ', 'd');
		StringBuilder builder = new StringBuilder();

		for (char currentChar : normalized.toCharArray()) {
			Character replacementChar = charReplacementMap.get(currentChar);
			builder.append(replacementChar != null ? replacementChar : currentChar);
		}
		
		return builder.toString();
	}
	
	public static String caseDiacriticReplace(String input){
		List<Character> vnDiacriticCharacter = new ArrayList<Character>();
		vnDiacriticCharacter.add('a');
		vnDiacriticCharacter.add('i');
		vnDiacriticCharacter.add('e');
		vnDiacriticCharacter.add('u');
		vnDiacriticCharacter.add('o');
		vnDiacriticCharacter.add('y');
		
		String diacriticIgnore = caseDiacriticIgnore(input);
		StringBuilder builder = new StringBuilder();
		char[] original  = input.toCharArray();
		int index  = 0;
		for (char currentChar : diacriticIgnore.toCharArray()) {
			builder.append((currentChar == original[index] && !vnDiacriticCharacter.contains(currentChar)) ? currentChar : "_");
			index++;
		}
		
		return builder.toString();
	}
	
	public static String fixPredict(String input){
		String tmpInput = input;
		tmpInput.replace("I-I","H");
		tmpInput.replace("i-i","H");
		Map<Character, Character> charReplacementMap = new HashMap<Character, Character>();
		charReplacementMap.put('0', 'o');
		charReplacementMap.put('ß', 'B');
		charReplacementMap.put('"', ' ');
		charReplacementMap.put('”', ' ');
		charReplacementMap.put('\'', ' ');
		charReplacementMap.put('.', ' ');
		// charReplacementMap.put(',', ' '); rem for multi food selection on one line
		charReplacementMap.put('*', ' ');
		charReplacementMap.put('?', ' ');
		StringBuilder builder = new StringBuilder();

		for (char currentChar : tmpInput.toCharArray()) {
			Character replacementChar = charReplacementMap.get(currentChar);
			if (replacementChar != null && replacementChar.equals(' ')){
			}
			else{
				builder.append(replacementChar != null ? replacementChar : currentChar);
			}
		}
		
		tmpInput = VietnameseDiacriticIgnore(builder.toString());
		
		String[] mySplit = tmpInput.split(" ");
		String begin = mySplit[0];
		builder = null;
		
		boolean iLower = false;
		boolean iIdentifi = false;
		StringBuilder beginbuilder = new StringBuilder();
		for (char curChar : begin.toCharArray()) {
			if (!iIdentifi){
				if (Character.isLowerCase(curChar) && !iLower){
					iLower = true;
				}
				if (Character.isUpperCase(curChar) && iLower){
					beginbuilder.append(curChar);
					iIdentifi = true;
				}
			}
			else{
				beginbuilder.append(curChar);
			}
		}
		if (iIdentifi){
			begin = beginbuilder.toString();
			if (mySplit.length == 1)
				return begin;
			else{
				String tail = begin;
				for (int i = 1; i < mySplit.length; i++)
					tail += " " + mySplit[i];
				return tail;
			}
		}
		else{
			return tmpInput.toLowerCase();
		}
	}
	
	public static String VietnameseDiacriticIgnore(String input){
		StringBuilder builder = new StringBuilder();
		List<Character> vnDiacriticCharacter = new ArrayList<Character>();
		vnDiacriticCharacter.add('a');
		vnDiacriticCharacter.add('i');
		vnDiacriticCharacter.add('e');
		vnDiacriticCharacter.add('u');
		vnDiacriticCharacter.add('o');
		vnDiacriticCharacter.add('y');
		
		String tmpInput = Normalizer.normalize(input, Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		
		for (char currentChar : tmpInput.toCharArray()) {
			builder.append(vnDiacriticCharacter.contains(currentChar) ? '%' : currentChar);
		}
		
		String normalized = Normalizer.normalize(builder.toString(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		StringBuilder finalBuilder = new StringBuilder();
		char[] original  = input.toCharArray();
		int index  = 0;
		for (char curChar : normalized.toCharArray()) {
			finalBuilder.append(curChar == '%' ? original[index] : curChar);
			index++;
		}
		
		return finalBuilder.toString();
	}
	
	public static boolean resultOCRCheck(String input){
		String[] mySplit = input.split("\n");
		if (input.contains("\n")){
			if (mySplit.length > 3){
				return false;
			}
			else{
				CaptureCamera.mInstance.isMultiLine = true;
				return true;
			}
		}
		if (input.length() < 4){
			return false;
		}
		return true;
	}

}