package com.hcmus.maths.textdetector;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class DetectorBaseHelper extends SQLiteOpenHelper {

	// Food table name
	private final String foodTableName = "MonAn";

	// The food table columns
	private final String nameVie = "ten";
	private final String desrptVie = "cachdung";
	private final String nameEng = "title";
	private final String desrptEng = "use";
	private final String keyIngre = "keyingre";
	private final String mainKey = "mainkey";

	private static String DB_PATH;

	private static String DB_NAME = "myandroid.db";

	private SQLiteDatabase myDataBase;

	private final Context myContext;

	/**
	 * Constructor Takes and keeps a reference of the passed context in order to
	 * access to the application assets and resources.
	 * 
	 * @param context
	 */
	public DetectorBaseHelper(Context context) {

		super(context, DB_NAME, null, 1);
		DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
		this.myContext = context;
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own
	 * database.
	 * */
	public void createDataBase() throws IOException {

		boolean dbExist = checkDataBase();

		if (dbExist) {
			// do nothing - database already exist
		} else {

			// By calling this method and empty database will be created into
			// the default system path
			// of your application so we are gonna be able to overwrite that
			// database with our database.
			this.getReadableDatabase();

			try {

				copyDataBase();

			} catch (IOException e) {

				throw new Error("Error copying database");

			}
		}

	}

	/**
	 * Check if the database already exist to avoid re-copying the file each
	 * time you open the application.
	 * 
	 * @return true if it exists, false if it doesn't
	 */
	private boolean checkDataBase() {

		SQLiteDatabase checkDB = null;

		try {
			String myPath = DB_PATH + DB_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READONLY);

		} catch (SQLiteException e) {

			// database does't exist yet.

		}

		if (checkDB != null) {

			checkDB.close();

		}

		return checkDB != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created
	 * empty database in the system folder, from where it can be accessed and
	 * handled. This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException {

		// Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		// Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		// transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}

		// Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();

	}

	public void openDataBase() throws SQLException {

		// Open the database
		String myPath = DB_PATH + DB_NAME;
		myDataBase = SQLiteDatabase.openDatabase(myPath, null,
				SQLiteDatabase.OPEN_READONLY);

	}

	@Override
	public synchronized void close() {

		if (myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	public List<String> queryFood(String queryName) {
		List<String> result = new ArrayList<String>();
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {

			Cursor cursor = myDataBase.query(foodTableName,
					new String[] { nameVie }, nameVie
							+ " LIKE ? COLLATE NOCASE", new String[] { "%"
							+ queryName + "%" }, null, null, null, null);

			for (int i = 0; i < cursor.getCount(); i++) {
				cursor.moveToNext();
				result.add(cursor.getString(0));
			}
			cursor.close();
		}
		return result;
	}

	public QueryResult getQueryResult(String queryName) {
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {
			Cursor cursor = myDataBase.query(foodTableName, new String[] {
					nameVie, desrptVie, nameEng, desrptEng }, nameVie
					+ "=? COLLATE NOCASE", new String[] { queryName }, null,
					null, null, null);
			cursor.moveToNext();
			return new QueryResult(cursor.getString(0), cursor.getString(1),
					cursor.getString(2), cursor.getString(3));
		}
		return null;
	}

	public List<String> queryIngreAndMain(String stringreKey, String strmainKey) {
		List<String> result = new ArrayList<String>();
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {

			Cursor cursor = null;
			if (stringreKey != null && strmainKey != null) {
				cursor = myDataBase.query(foodTableName, new String[] {
						nameVie, desrptVie, nameEng, desrptEng }, keyIngre
						+ "=? COLLATE NOCASE AND " + mainKey
						+ "=? COLLATE NOCASE", new String[] { stringreKey, strmainKey }, null,
						null, null, null);
			} else if (stringreKey != null) {
				cursor = myDataBase.query(foodTableName, new String[] {
						nameVie, desrptVie, nameEng, desrptEng }, keyIngre
						+ "=? COLLATE NOCASE", new String[] { stringreKey }, null,
						null, null, null);
			} else {
				cursor = myDataBase.query(foodTableName, new String[] {
						nameVie, desrptVie, nameEng, desrptEng }, mainKey
						+ "=? COLLATE NOCASE", new String[] { strmainKey }, null,
						null, null, null);
			}
			if (cursor != null && cursor.moveToFirst()) {
				do {
					if (cursor.getString(0) != null) {
						result.add(cursor
								.getString(0));
					}
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		return result;
	}

	public List<String> queryIngreKey() {
		List<String> result = new ArrayList<String>();
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {
			Cursor cursor = myDataBase.query(foodTableName,
					new String[] { keyIngre }, null, null, null, null, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					if (cursor.getString(0) != null
							&& !result.contains(cursor.getString(0))
							&& !cursor.getString(0).isEmpty()) {
						result.add(cursor
								.getString(0));
					}
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		return result;
	}

	public List<String> queryMainKey() {
		List<String> result = new ArrayList<String>();
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {
			Cursor cursor = myDataBase.query(foodTableName,
					new String[] { mainKey }, null, null, null, null, null);

			if (cursor != null && cursor.moveToFirst()) {
				do {
					if (cursor.getString(0) != null
							&& !result.contains(cursor.getString(0))
							&& !cursor.getString(0).isEmpty()) {
						result.add(cursor.getString(0));
					}
				} while (cursor.moveToNext());
			}
			cursor.close();
		}
		return result;
	}

	public List<String> queryPreDictFood(String queryName) {
		List<String> result = new ArrayList<String>();
		String likeTextQuery = TextUtil.caseDiacriticReplace(queryName);
		if (myDataBase.isOpen() && myDataBase.isReadOnly()) {
			Cursor cursor = myDataBase.query(foodTableName,
					new String[] { nameVie }, nameVie
							+ " LIKE ? COLLATE NOCASE", new String[] { "%"
							+ likeTextQuery + "%" }, null, null, null, null);

			for (int i = 0; i < cursor.getCount(); i++) {
				cursor.moveToNext();
				result.add(cursor.getString(0));
			}
			cursor.close();
		}
		return result;
	}

	// public void queryFoodLike(String queryName){
	// if (myDataBase.isOpen() && myDataBase.isReadOnly()){
	// Cursor cursor = myDataBase.query(foodTableName, new String[]{nameVie,
	// desrptVie, nameEng, desrptEng, imageName},
	// nameVie + " LIKE ? OR " + nameEng +" LIKE ? COLLATE NOCASE" , new
	// String[]{"%" + queryName + "%","%" + queryName + "%"}, null, null, null,
	// null);
	// cursor.moveToNext();
	// cursor.moveToNext();

	// Bitmap cd = BitmapFactory.decodeByteArray(cursor.getBlob(3), 0,
	// cursor.getBlob(3).length);
	// }
	// }

	// public List<String> queryPredict(String queryName){
	// if (myDataBase.isOpen() && myDataBase.isReadOnly()){
	// Cursor cursor = myDataBase.query(foodTableName, new String[]{nameVie,
	// nameEng},
	// nameVie + " LIKE ? OR " + nameEng +" LIKE ? COLLATE NOCASE" , new
	// String[]{"%" + queryName + "%","%" + queryName + "%"}, null, null, null,
	// null);
	// }
	// }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	// Add your public helper methods to access and get content from the
	// database.
	// You could return cursors by doing "return myDataBase.query(....)" so it'd
	// be easy
	// to you to create adapters for your views.

}