package com.hcmus.maths.textdetector;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;


public class PredictQuery {

	private List<String> foodNameVie;
	private List<String> foodNameEng;

	public PredictQuery(
	List<String> foodNameVieIn,
	List<String> foodNameEngIn) {
		this.foodNameVie = foodNameVieIn;
		this.foodNameEng = foodNameEngIn;
	}
	
	public void setVie(List<String> input){
		this.foodNameVie = input;
	}
	
	public void setEng(List<String> input){
		this.foodNameEng = input;
	}
	
	public List<String> getVie(){
		return this.foodNameVie;
	}
	
	public List<String> getEng(){
		return this.foodNameEng;
	}
}
