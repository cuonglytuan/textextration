package com.hcmus.maths.textdetector;

import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.graphics.BitmapFactory;

final class TextDetectorAsyncTask extends AsyncTask<Void, Void, Boolean> {

	private CaptureCamera activity;
	private TessBaseAPI baseApi;
	private Bitmap sources;
	private byte[] data;
	private int width;
	private int height;
	private OcrResult ocrResult;
	private long timeRequired;

	TextDetectorAsyncTask(CaptureCamera activity, TessBaseAPI baseApi, byte[] data, int width, int height) {
		this.activity = activity;
		this.baseApi = baseApi;
		this.data = data;
		this.width = width;
		this.height = height;
	}
  
	TextDetectorAsyncTask(CaptureCamera activity, TessBaseAPI baseApi, Bitmap sources) {
		this.activity = activity;
		this.baseApi = baseApi;
		this.sources = sources;
	}

	@Override
	protected Boolean doInBackground(Void... arg0) {
		long start = System.currentTimeMillis();
		String textResult;

		try {     
			baseApi.setImage(sources);
			textResult = baseApi.getUTF8Text();
			timeRequired = System.currentTimeMillis() - start;

			// Check for failure to recognize text
			if (textResult == null || textResult.equals("")) {
				return false;
			}
			ocrResult = new OcrResult();
			ocrResult.setWordConfidences(baseApi.wordConfidences());
			ocrResult.setMeanConfidence( baseApi.meanConfidence());
			ocrResult.setRegionBoundingBoxes(baseApi.getRegions().getBoxRects());
			ocrResult.setTextlineBoundingBoxes(baseApi.getTextlines().getBoxRects());
			ocrResult.setWordBoundingBoxes(baseApi.getWords().getBoxRects());
			/*ocrResult.setCharacterBoundingBoxes(baseApi.getCharacters().getBoxRects());
			ocrResult.setThresholdedImage(baseApi.GetThresholdedImage());
			ocrResult.setGreyImage(baseApi.GetGreyImage());*/
		} catch (RuntimeException e) {
			Log.e("OcrRecognizeAsyncTask", "Caught RuntimeException in request to Tesseract. Setting state to CONTINUOUS_STOPPED.");
			e.printStackTrace();
			try {
				baseApi.clear();
				// activity.stopHandler();
			} catch (NullPointerException e1) {
				// Continue
			}
			return false;
		}
		timeRequired = System.currentTimeMillis() - start;
		ocrResult.setBitmap(sources);
		ocrResult.setText(textResult);
		ocrResult.setRecognitionTimeRequired(timeRequired);
		return true;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		// if (result){
			// activity.dismissDialog();
			// activity.showResultDialog();
		// }
		Handler handler = activity.getHandler();
		if (handler != null) {
			// Send results for single-shot mode recognition.
			if (result) {
				Message message = Message.obtain(handler, activity.TEXT_EXTRACT_SUCCESS, ocrResult);
				message.sendToTarget();
			} else {
				Message message = Message.obtain(handler, activity.TEXT_EXTRACT_FAIL, ocrResult);
				message.sendToTarget();
			}
			activity.dismissDialog();
		}
		if (baseApi != null) {
			baseApi.clear();
		}
	}
}
