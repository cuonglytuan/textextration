package com.hcmus.maths.textdetector;

import java.util.List;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;

public class QueryResult {

	private Bitmap foodImage;
	
	private String foodNameVie;
	private String foodNameEng;
	private String foodInsVie;
	private String foodInsEng;

	public QueryResult(Bitmap IfoodImage,
	String IfoodNameVie,
	String IfoodInsVie,
	String IfoodNameEng,
	String IfoodInsEng
	) {
		this.foodImage = IfoodImage;
		
		this.foodNameVie = IfoodNameVie;
		this.foodNameEng = IfoodNameEng;
		this.foodInsVie = IfoodInsVie;
		this.foodInsEng = IfoodInsEng;
	}
	
	public QueryResult(String IfoodNameVie,
	String IfoodInsVie,
	String IfoodNameEng,
	String IfoodInsEng
	) {
		
		this.foodNameVie = IfoodNameVie;
		this.foodNameEng = IfoodNameEng;
		this.foodInsVie = IfoodInsVie;
		this.foodInsEng = IfoodInsEng;
	}


	public Bitmap getBitmap() {
		return this.foodImage;
	}

	public String getVieTitle() {
		return this.foodNameVie;
	}
	
	public String getEngTitle() {
		return this.foodNameEng;
	}
	
	public String getVieIns() {
		return this.foodInsVie;
	}
	
	public String getEngIns() {
		return this.foodInsEng;
	}
	
	public void setBitmap(Bitmap image) {
		this.foodImage = image;
	}

	public void setVieTitle(String input) {
		this.foodNameVie = input;
	}
	
	public void setEngTitle(String input) {
		this.foodNameEng = input;
	}
	
	public void setVieIns(String input) {
		this.foodInsVie = input;
	}
	
	public  void setEngIns(String input) {
		this.foodInsEng = input;
	}

}
