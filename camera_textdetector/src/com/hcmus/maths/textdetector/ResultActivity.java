package com.hcmus.maths.textdetector;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public final class ResultActivity extends Activity {

	public static ResultActivity mInstance;
	private TextView  fTitle, fDes;
	private ImageView fImage;
	
	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mInstance = this;
		if (CaptureCamera.mInstance.queryResult != null && CaptureCamera.mInstance.isUnique){

			setContentView(R.layout.food_result_page);
			
			fImage = (ImageView) findViewById(R.id.foodImage);
			String resImage = TextUtil.caseDiacriticIgnore(CaptureCamera.mInstance.queryResult.getVieTitle());
			resImage = resImage.toLowerCase();
			resImage = resImage.replace(" ", "");
			Log.d("my debug re", resImage);
			try{
				Drawable myDraw = mInstance.getResources().getDrawable(getResourcesId(resImage));
				fImage.setBackgroundDrawable(myDraw);
			} catch (Exception e){
				Drawable myDraw = mInstance.getResources().getDrawable(getResourcesId("unknownimage"));
				fImage.setBackgroundDrawable(myDraw);
			}

			fTitle = (TextView) findViewById(R.id.foodTitle);
			fTitle.setText((CaptureCamera.mInstance.langIndex == 0 ? (CaptureCamera.mInstance.queryResult.getEngTitle() + "\n(" + CaptureCamera.mInstance.queryResult.getVieTitle() +")") : 
			(CaptureCamera.mInstance.queryResult.getVieTitle() + "\n(" + CaptureCamera.mInstance.queryResult.getEngTitle() +")")));
			
			fDes = (TextView) findViewById(R.id.foodText);
			fDes.setText((CaptureCamera.mInstance.langIndex == 0 ? CaptureCamera.mInstance.queryResult.getEngIns() : CaptureCamera.mInstance.queryResult.getVieIns()));
		}
		else{
			// if (Camera.mInstance.queryList.size()  < 2){
				// new DatabaseQueryAsyncTask(Camera.mInstance, Camera.mInstance.myDbHelper, Camera.mInstance.queryList.get(0)).execute();
			// }
			// else {
				setContentView(R.layout.food_result_multi);
				ListView mainListView = (ListView) findViewById( R.id.list_result );
				mainListView.setClickable(true);
				ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(CaptureCamera.mInstance, R.layout.simplerow, CaptureCamera.mInstance.queryList);
				mainListView.setAdapter( listAdapter );
				mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> a, View v, int position, long id) {
						TextView myText = (TextView) v.findViewById(R.id.rowTextView);
						new DatabaseQueryAsyncTask(CaptureCamera.mInstance, CaptureCamera.mInstance.myDbHelper, myText.getText().toString()).execute();
					}
				});
			// }
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		return super.onKeyDown(keyCode, event);
	}
	public int getResourcesId(String uri){
		return mInstance.getResources().getIdentifier("drawable/" + uri, null, mInstance.getPackageName());
	}
}