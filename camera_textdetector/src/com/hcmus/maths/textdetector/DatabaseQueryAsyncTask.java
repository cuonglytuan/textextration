package com.hcmus.maths.textdetector;

import java.util.List;

import com.googlecode.leptonica.android.ReadFile;
import com.googlecode.tesseract.android.TessBaseAPI;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.graphics.BitmapFactory;

final class DatabaseQueryAsyncTask extends AsyncTask<Void, Void, Boolean> {

	private CaptureCamera activity;
	private DetectorBaseHelper myDbHelper;
	private String qText;
	private String inKey = null;
	private String maKey = null;
	private QueryResult myQueryResult;
	private List<String> preResult;
	private int preProcessTime = 0;
  
	DatabaseQueryAsyncTask(CaptureCamera activity, DetectorBaseHelper myDbHelper, String qText) {
		this.activity = activity;
		this.myDbHelper = myDbHelper;
		this.qText = qText;
		this.preProcessTime = 0;
	}
	
	DatabaseQueryAsyncTask(CaptureCamera activity, DetectorBaseHelper myDbHelper, String qText, int preTime) {
		this.activity = activity;
		this.myDbHelper = myDbHelper;
		this.qText = qText;
		this.preProcessTime = preTime;
	}	

	DatabaseQueryAsyncTask(CaptureCamera activity, DetectorBaseHelper myDbHelper, 
	String qText, int preTime, String insKey, String masKey) {
		this.activity = activity;
		this.myDbHelper = myDbHelper;
		this.qText = qText;
		this.preProcessTime = preTime;
		this.inKey = insKey;
		this.maKey = masKey;
	}

	@Override
	protected Boolean doInBackground(Void... arg0) {
	
		try {     
			myDbHelper.openDataBase();
			if (inKey != null || maKey != null){
				Log.i("tuancuong", "inKey " + inKey);
				Log.i("tuancuong", "maKey " + maKey);
				preResult = myDbHelper.queryIngreAndMain(inKey, maKey);
				if(!preResult.isEmpty() && preResult != null){
					// key word query from ocr result					
					myDbHelper.close();
					return true;
				} else {
					myDbHelper.close();
					return false;
				}
			} else {
				preResult = myDbHelper.queryFood(qText);
				if(!preResult.isEmpty() && preResult != null){
					// exact query from ocr result
					if (preResult.size() == 1)
					{
						// query success only one result can find out
						myQueryResult = myDbHelper.getQueryResult(qText);
						myDbHelper.close();
						return true;
					}
					else{
						myDbHelper.close();
						return true;
					}
				}
				else {
					preResult = myDbHelper.queryPreDictFood(qText);
					if(!preResult.isEmpty() && preResult != null){
						myDbHelper.close();
						return true;
					}
					else {
						myDbHelper.close();
						return false;
					}
				}
			}
			
		} catch (RuntimeException e) {
			try {
				myDbHelper.close();
				// activity.stopHandler();
			} catch (NullPointerException e1) {
				// Continue
			}
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean result) {
		super.onPostExecute(result);
		Handler handler = activity.getHandler();
		if (handler != null) {
			if (result) {
				if (myQueryResult != null){
					Message message = Message.obtain(handler, activity.QUERY_UNIQUE_RESULT, myQueryResult);
					message.sendToTarget();
				}
				else if(preResult != null && !preResult.isEmpty()){
					Message message = Message.obtain(handler, activity.QUERY_MULTI_RESULT, preResult);
					message.sendToTarget();
				}
				else{
					Message message = Message.obtain(handler, activity.QUERY_FAIL_RESULT, null);
					message.sendToTarget();
				}
			} else {
				if (preProcessTime < 1){
					Message message = Message.obtain(handler, activity.QUERY_FAIL_RESULT, null);
					message.sendToTarget();
				}
				else if (preProcessTime < 2){
					Message message = Message.obtain(handler, activity.QUERY_FAIL_RESULT_SECOND, null);
					message.sendToTarget();
				}
				else if (preProcessTime < 3){
					Message message = Message.obtain(handler, activity.QUERY_FAIL_RESULT_KEY_WORD, null);
					message.sendToTarget();
				}
				else{
					Message message = Message.obtain(handler, activity.QUERY_FAIL_RESULT_FINAL, null);
					message.sendToTarget();
				}
			}
		}
	}
}
