package com.hcmus.maths.textdetector;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public final class LanguageActivity extends Activity {
	
	private ListView mainListView;
	private ArrayAdapter<String> listAdapter;
	private List<String> list;
	public static LanguageActivity mInstance;
	
	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		mInstance = this;
		list = new ArrayList<String>();
		list.add("English");
		list.add("Tiếng Việt");
		setContentView(R.layout.language_selection);
		mainListView = (ListView) findViewById( R.id.list_lang);
		mainListView.setClickable(true);
		listAdapter = new ArrayAdapter<String>(this, R.layout.language_row, list);
		mainListView.setAdapter( listAdapter );
		mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int position, long id) {
				mInstance.startActivityForResult(new Intent(mInstance, CaptureCamera.class), 0x6);
				CaptureCamera.mInstance.setLanguage(position);
				mInstance.finish();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	
		return super.onKeyDown(keyCode, event);
	}
}