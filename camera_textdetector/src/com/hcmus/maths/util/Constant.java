package com.hcmus.maths.util;

import android.content.SharedPreferences;

public class Constant {

	public static final boolean VIEW_DEBUG_RESULT = true;
	public static final boolean SAVE_CAPTURE_ZONE = true;
	public static final String DEBUG_IMAGE_PATH = "/mnt/sdcard/tesseract/capture/";
}
